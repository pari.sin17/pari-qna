import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { AnswerPage } from '../answer/answer';
/**
 * Generated class for the BrowsePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-browse',
  templateUrl: 'browse.html',
})
export class BrowsePage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }
  i=0;
  ionViewDidLoad() {
    console.log('ionViewDidLoad BrowsePage');
  }

  increment(){
    this.i++;
  }

  decrement(){
    this.i--;
  }
  view(){
    this.navCtrl.push(AnswerPage)

  }
 
}
